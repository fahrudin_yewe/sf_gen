<?php

//GET SEMUA DATA APLIKASI
function data_app($id = 'APP_NAME')
{
    $ci            = &get_instance();
    $data_instansi = $ci->db->query("SELECT conf_val FROM sy_config WHERE conf_name='$id'")->row();
    return $data_instansi->conf_val;
}

//GET DATA USERS
function users($atr='username')
{
    $ci            = &get_instance();
    $id = $ci->session->userdata('id_user');
    $data = $ci->db->query("SELECT * FROM sy_user WHERE id=$id")->row();
    return isset($data->$atr)?$data->$atr:"";
}


//SET LAYOUT NAME
function layout($l = 'back')
{
    if ($l == 'front') {
        return "layout_frontend";
    } else {
        return "layout_backend";
    }
}

//MENAMBAHKAN CLASS ACTIVE PADA MENU
function activate_menu($controller, $by = 'c')
{
    //c=controller, m=method
    // Getting CI class instance.
    $CI = get_instance();
    // Getting router class to active.
    if ($by == 'c') {
        $class = $CI->router->fetch_class();
    } elseif ($by == 'm') {
        $class = $CI->router->fetch_method();
    }
    return ($class == $controller) ? 'active' : '';
}

//FORMAT RUPIAH
function format_rupiah($number)
{

    return 'Rp ' . number_format($number);
}

function formatBytes($size, $precision = 2)
{
    $base     = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
}

//GET NUM ROWS
function get_numrows($tbl)
{
    $ci = &get_instance();
    $ci->db->select('*');
    $total_row = $ci->db->get($tbl)->num_rows();
    return $total_row;
}

//HTML LOOKUP
function lookup()
{?>
<div class="modal" id="lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                        &times;
                    </span>
                    </button>
                </div>
                <div class="modal-body">
                    <h1><i class="fa fa-refresh"></i></h1>
                </div>
            </div>
        </div>
    </div>
<?php }?>